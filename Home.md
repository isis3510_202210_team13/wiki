## Team 13

- Juan Fernando Castañeda Gutiérrez - jf.castaneda@uniandes.edu.co - https://gitlab.com/MightopitaGuy
- Juan Felipe Peña Criado - jf.pena10@uniandes.edu.co - https://gitlab.com/jf.pena10 
- Santiago Ballesteros Pastrán - s.ballesteros@uniandes.edu.co - https://gitlab.com/sballesteros99
- Juan Pablo Romero Peña - jp.romero12@uniandes.edu.co - https://gitlab.com/JuanPabloRomero 
- Daniel Alejandro Ángel Fuertes - da.angelf@uniandes.edu.co - https://gitlab.com/danr844
- Ángel Antonio Rodríguez Varela - aa.rodriguezv@uniandes.edu.co - https://gitlab.com/aa-rodriguezv

## Table of content

0. [Contract](Contract.md)

1. Sprint 1
    - [MS2](Sprint1/MS2.md)

[Go to the top](#team-13)
